﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiClase3.Models;

namespace ApiClase3.Controllers
{
    public class PersonaController : ApiController
    {
        // GET: api/Persona
        public IHttpActionResult Get()
        {
            mi_base_de_datosEntities midb = new mi_base_de_datosEntities();
            List<Persona> lista_personas = midb.Persona.ToList();
            return Ok(lista_personas);
        }

        // GET: api/Persona/5
        public IHttpActionResult Get(int id)
        {
            //-----
            mi_base_de_datosEntities midb = new mi_base_de_datosEntities();
            Persona persona_encontrada = midb.Persona.Find(id);
            if(persona_encontrada == null)
            {
                return NotFound();
            }
            return Ok(persona_encontrada);
        }

        // POST: api/Persona
        public IHttpActionResult Post([FromBody]Persona perona_insertada)
        {
            mi_base_de_datosEntities midb = new mi_base_de_datosEntities();
            midb.Persona.Add(perona_insertada);
            midb.SaveChanges();
            return Ok();
        }

        // PUT: api/Persona/5
        public IHttpActionResult Put(int id, [FromBody]Persona persona_modificada)
        {
            mi_base_de_datosEntities midb = new mi_base_de_datosEntities();
            Persona persona_db = midb.Persona.Find(id);
            if (persona_db == null)
            {
                return NotFound();
            }
            persona_db.Nombre = persona_modificada.Nombre;
            persona_db.Apellido = persona_modificada.Apellido;
            persona_db.Telefono = persona_modificada.Telefono;
            //midb.Persona.
            midb.SaveChanges();
            return Ok();
        }

        // DELETE: api/Persona/5
        public IHttpActionResult Delete(int id)
        {
            mi_base_de_datosEntities midb = new mi_base_de_datosEntities();
            Persona persona_db = midb.Persona.Find(id);
            if (persona_db == null)
            {
                return NotFound();
            }
            midb.Persona.Remove(persona_db);
            midb.SaveChanges();
            return Ok();
        }
    }
}
